import React from "react"
import { Button, Grid , List, ListItem, ListItemText, Divider} from '@material-ui/core';
import {Edit , Delete } from '@material-ui/icons';
import { render } from "react-dom";




const EditTodo  = ({todo}) => {

    console.log(todo); 
  
    <form className="stack-small">
      <div className="form-group">
        <label className="todo-label" htmlFor={todo.id}>
          Editing a task 
        </label>
        <input id={todo.id} className="todo-text" type="text" value= {todo.text} />
      </div>
      <div className="btn-group">
        <Button type="button" className="btn todo-cancel" >
          Cancel
          
        </Button>
        <Button type="submit" className="btn btn__primary todo-edit">
          Save
        </Button>
      </div>
    </form>
  
  }


export default EditTodo
  