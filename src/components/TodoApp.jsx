import { Button, Grid} from '@material-ui/core';
import React  from 'react';
import styles from './style.css'
import TodoList from './TodoList';

// Le composante pour ajouter une tâche
class TodoApp extends React.Component{

    constructor(props){
  
      super(props); 
      this.state = {
        todos : [],
        newTodo : '',
        idEditing : '-1', 
        textEditing : ""
      }
  
  
      // The binding 
      this.addTodo = this.addTodo.bind(this);
      this.handleChange = this.handleChange.bind(this);  
      this.handleComplete = this.handleComplete.bind(this); 
      this.handleRemove = this.handleRemove.bind(this);
      this.handleAll = this.handleAll.bind(this); 
      this.handleDisplayComplete = this.handleDisplayComplete.bind(this); 
      this.handleDisplayNotComplete = this.handleDisplayNotComplete.bind(this); 
      this.setEditingId = this.setEditingId.bind(this); 
      this.handleChangeEditing = this.handleChangeEditing.bind(this); 
      this.updateTodo = this.updateTodo.bind(this); 
    }
  
  
    handleChange(e){
        if(e.target.value !== ""){
            this.setState ({newTodo : e.target.value})
        } 
    }
  
    // The function addTodo 
    addTodo(e){
      e.preventDefault(); 
      let todoss = this.state.todos ; 
  
     // (todos.length > 0 ) ? todo.id = 1 : todo.id = todos.length + 1 ;
      let todo = {
        id : (todoss.length < 1 ) ? 1 : todoss.length + 1 , 
        text : this.state.newTodo , 
        isComplete : 0
      }
  
      this.setState( state => ({
        todos : todo.text.replace(/\s/g,"") !== "" ? this.state.todos.concat({todo}) : this.state.todos, 
        newTodo : ''
      }));
  
      
    }
  
    handleComplete(id){
      let  todos  = this.state.todos;
      let item, todos_array 
      todos_array = todos 
   
      const todosCopy = [...todos] 
      const index = todosCopy.findIndex(item => item.todo.id === id) ;
  
      if (index !== undefined) {
         var complete = todosCopy[index].todo.isComplete ;

       ( complete === 0 ) ? todosCopy[index].todo.isComplete = 1 : todosCopy[index].todo.isComplete = 0 ; 
      
      }
  
      this.setState ({todos : todosCopy})
    }
  
    handleRemove(id){
        this.setState ( state => ({
        todos : this.state.todos.filter( 
          t => t.todo.id !== id) , 
      }))
    }
  
  
    handleAll(){
      let todos = this.state.todos ; 
      todos = todos.filter (
        t => (t.todo.isComplete)
      )
  
      return todos ; 
  
    }
  
    handleDisplayComplete(){
  
      let todos = this.state.todos ; 
  
      this.setState( state => ({
        filter : 'completed'
      })); 
  
      return todos ;
    }
  
    handleDisplayNotComplete(){
      let todos ; 
      todos = this.state.todos.filter( 
          t => (t.todo.isComplete === 0) ) 
          
      return todos ; 
      
    }
  
  
    // Updating 
    // Garder le paramètre qui fait l'édition et changer le text Editing
    setEditingId(id){

      let  todos = this.state.todos ; 

      var text ;  
      var todosCopy = [...todos]  ; 
      const index = todosCopy.findIndex(item => item.todo.id === id) ;

      if (index !== undefined) {
      text = todosCopy[index].todo.text ; 
    
    }

      this.setState ( state => ({
        idEditing : id , 
        textEditing : text
      })); 

      console.log(this.state.textEditing); 
    }


    // Garder une trace du changement de view  
    handleChangeEditing(e){
      if(e.target.value !== ""){
        this.setState ({textEditing : e.target.value})
      }
    }


    // Mettre à jour au clic du 
    updateTodo(id){
        
        let  todos = this.state.todos ; 

        var text = this.state.textEditing; 
        var todosCopy = [...todos]  ; 
        const index = todosCopy.findIndex(item => item.todo.id === id) ;
  
        if (index !== undefined) {
        todosCopy[index].todo.text = text 
      
      }
  
      this.setState ({
        todos : todosCopy, 
        textEditing : '',
        idEditing : '-1'
      })
    }
  
    render(){
      let {todos, newTodo, id, textEdit } = this.state ; 

       return(
        
        <div className="todoapp">
          
          <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems = "center"
            
          >
            <div className="todoAppContainer">
            <h2 className="todoAppTitle" alignItems = "center">TodoList App</h2>
              <br/>
              <form >
                <input type= "text" value={newTodo}  onChange={this.handleChange} placeholder = "Add a new task..." className="todoInput" /> 
                <Button variant="contained" color="#b52ef4" className="addButton"  type="submit" onClick= {this.addTodo}>AJOUTER</Button>
              </form>
  
  
              {/* Afficher la liste des tâches */}
              <div>
        
              <TodoList 
                todos={todos} onComplete = {this.handleComplete}  onRemove = {this.handleRemove} onEditing = {this.setEditingId} handleEditing={this.handleChangeEditing} endEditing={this.updateTodo} EditingId = {this.state.idEditing} EditingText = {this.state.textEditing}/>
  
              </div>
            </div>
          
          </Grid>
        </div>
       ); 
    }
  }
  
  
export default TodoApp ;   