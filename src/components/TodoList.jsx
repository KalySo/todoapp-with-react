import React  from "react"
import { Button , List, ListItem, Divider} from '@material-ui/core';
import {Edit , Delete } from '@material-ui/icons';



const TodoList = ({todos, onComplete, onRemove, onEditing, handleEditing,  endEditing, EditingId,  EditingText}) => { 


    return (
    <ul>
    {todos
   
    .map(item => (
      
      <div>
        <List>
          <Divider className="todoList"/>
          <ListItem>
            <p className="todoList" alignItems="center">
              { (EditingId  === item.todo.id) ? 
              (
                  <div className="EditingItem">
                  <br/><br/>
                  <label>Editer la tâche : </label>
                  <input type="text" value ={EditingText} onChange={handleEditing}></input>
                  <Button className="updateButton"  onClick ={ () => endEditing(item.todo.id)} >  Enregistrer</Button>
                </div>

              ) : 
              (
                <div className="ListItems" >
                  <input type="checkbox" id={item.todo.id}  onChange={() => onComplete(item.todo.id)} />
                  <label key={item.todo.id} className = {(item.todo.isComplete) ? "checked" : "not-checked"}  >{item.todo.text}</label>
                  <Button  onClick ={ () => onRemove(item.todo.id)} >  <Delete className="deleteIcon" /></Button>
                  <Button  color="default" onClick={() => onEditing(item.todo.id)} ><Edit /></Button> 
              </div>
              ) }
                
                
                

                
                
            </p>
          </ListItem>
          
        </List>
          
        
      </div>
      
    ))}
    </ul>
    )
  }


export default TodoList 