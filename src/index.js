// Les imports 
import React from 'react';
import TodoList from './components/TodoList';
import TodoApp from './components/TodoApp';
import editTaskView from './components/EditTodo';
import ReactDOM, { render } from 'react-dom';

import reportWebVitals from './reportWebVitals';






/*
const FILTER_MAP = {
  All: () => true,
  Active: todo => !todo.isComplete,
  Completed: todo => todo.isComplete
};

const FILTER_NAMES = Object.keys(FILTER_MAP);


const filterList = FILTER_NAMES.map(name => (
  <FilterButton key={name} name={name}/>
));


function FilterButton(props) {
  return (
    <button
      type="button"
      className="btn toggle-btn"
      aria-pressed={props.isPressed}
      
    >
      <span className="visually-hidden">Show </span>
      <span>{props.name}</span>
      <span className="visually-hidden"> tasks</span>
    </button>
  );
}

*/

/**
 * La composante pour le filtre 
 */

const FilterTask = ({filter}) => {

  return (
    <div>
      <ul className="filterboutons">
        <li><button className="active" onClick  >All</button></li>
        <li><button onClick >Completed</button></li>
        <li><button onClick  >Not completed</button></li>
      </ul>
    </div>
  )

}


const testComponent = () => {
  return (
    <h1>Hello I am a test</h1>
  )
}



ReactDOM.render(
  
  <React.StrictMode>

  < TodoApp />

  <testComponent />
    
  </React.StrictMode>,
  
  document.getElementById('root')
);
